package jvm_challenge

import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import java.io.BufferedReader
import java.io.File
import java.io.FileNotFoundException
import java.io.FileReader

private fun getHtmlFromNerdery(url: String, fileLocation: String) {
    val (request, response, result) = url.httpGet().responseString()
    result.fold({ d ->
        val text = scalpHtmlResults(result)
        saveText(fileLocation, text)
    }, { err ->
        //do nothing
    })
}

private fun scalpHtmlResults(html: Result<String, FuelError>): String {
    val text : StringBuilder = StringBuilder()
    val matches = """<h2>(.*?)<\/h2>""".toRegex()
            .findAll(html.toString())
            .map { it.value.replace("""<[^>]*>""".toRegex(), "") }
            .toList()
    println(matches)//so we only have to go to standard out once
    return(matches.toString())
}

private fun saveText(location: String, text: String) {
    try {
        File(location).printWriter().use { out ->
            out.println(text)
        }
    } catch (e: Exception){
        //do nothing
    }
}

private fun readFile(location: String) {
    val read = BufferedReader(FileReader(location))
    read.forEachLine { line -> println(line) }
}

fun main(args: Array<String>) {
    val fileLocation = "/tmp/rvanbelk.txt"
    val url = "https://nerdery.com/what-we-do"

    try {
        readFile(fileLocation)
    } catch (e: FileNotFoundException) {
        getHtmlFromNerdery(url, fileLocation)
    }
}
