# What We Do

This repository represents my solution to the Nerdery JVM Challenge, Issue #59

## Running

To run the application, simply invoke the provided JAR like so:

    java -jar <path/to/project>/dist/what_we_do_jvm_challenge-1.0-all.jar

## Building

To build the application, use the provided `gradlew` tool:

    ./gradlew clean shadowJar